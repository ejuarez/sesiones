console.log("Hola desde node");

//fs: filesystem
const filesystem = require('fs');
filesystem.writeFileSync('hola.txt', 'Hola desde node');

//Imprimir el arreglo en orden con código asíncrono
const arreglo = [5000, 60, 90, 100, 10, 20, 0, 120, 2000, 340, 1000, 50];
for (let item of arreglo) {
    setTimeout( () => {
        console.log(item);
    }, item);
}

const http = require('http');

const requestHandler = require('./routes');

const server = http.createServer( requestHandler );

server.listen(3000);

